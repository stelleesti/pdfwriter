FROM openjdk:17

COPY build/libs/PdfService-0.0.1.jar /usr/src/pdfService.jar

EXPOSE 8081

ENTRYPOINT ["java", "-jar", "/usr/src/pdfService.jar"]