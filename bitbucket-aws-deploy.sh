
pip3 install awscli
aws configure set aws_access_key_id "$AWS_KEY"
aws configure set aws_secret_access_key "$AWS_SECRET"
aws configure set default.region eu-west-1
eval $(aws ecr get-login --no-include-email --region eu-west-1 | sed 's;https://;;g')

docker build -f Dockerfile -t $AWS_DEPLOYMENT_NAME .

docker tag ${AWS_DEPLOYMENT_NAME}:latest 381768224218.dkr.ecr.eu-west-1.amazonaws.com/${AWS_DEPLOYMENT_NAME}:latest
docker push 381768224218.dkr.ecr.eu-west-1.amazonaws.com/${AWS_DEPLOYMENT_NAME}

aws ecs update-service --cluster $AWS_CLUSTER_NAME --service $AWS_SERVICE_NAME --force-new-deployment
