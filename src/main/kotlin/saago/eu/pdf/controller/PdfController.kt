package saago.eu.pdf.controller

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder
import org.springframework.context.ApplicationContext
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.expression.ThymeleafEvaluationContext
import saago.eu.pdf.PdfForm
import saago.eu.pdf.config.TemplateEngineConfig
import java.io.ByteArrayOutputStream
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@RestController
class PdfController(
        private val templateEngine: TemplateEngineConfig,
        private val applicationContext: ApplicationContext
) {
    @PostMapping("/act", produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun composeActPDF(
            @RequestBody form: PdfForm
    ): ResponseEntity<Any> {

        val thymeleafContext = Context()
        thymeleafContext.setVariable(ThymeleafEvaluationContext.THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME,
                ThymeleafEvaluationContext(applicationContext, null))
        thymeleafContext.setVariable("confirmedTask", form)

        val htmlTemplate = templateEngine.templateEngine().process("act", thymeleafContext)
        val outputStream = ByteArrayOutputStream()
        PdfRendererBuilder().withHtmlContent(htmlTemplate, null).toStream(outputStream).useFastMode().testMode(true).run()
        val file = ByteArrayResource(outputStream.toByteArray())
        val filename = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) + "_akt_${form.code}"
        return ResponseEntity.ok()
            .header("Content-Disposition", "attachment; filename=${filename}.pdf")
                .contentLength(file.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(file)
    }
}