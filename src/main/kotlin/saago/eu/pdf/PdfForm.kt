package saago.eu.pdf

import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.UUID


enum class TaskActTypeName {
    DEFAULT,
    EVERY_DEVICE_ON_NEW_PAGE,
    TOP_LEVEL_DEVICE_ON_NEW_PAGE
}

data class PdfForm(
        val id: UUID,
        val name: String,
        val code: Long,
        val description: String,
        val totalTime: Long? = null,
        val extraTime: Long? = null,
        val technicianComment: String?,
        val property: Property,
        val work: List<Work>,
        val totalWorkPrice : BigDecimal,
        val activities: List<Activity> = emptyList(),
        var materials: List<Material>,
        val displayType : TaskActTypeName,
        val deviceActivities: List<Activity> = emptyList(),
        val isSlaTask : Boolean,
        val billingType : String,
        val taskStartTime: LocalDateTime?
) {

}
data class Activity(
        val name: String,
        val completed: Boolean?,
        val orderNumber: Int,
        val activities: List<Activity>,
        val comment: String? = null,
        val measurements: List<ActivityMeasurement>
)

data class ActivityMeasurement(
        val value: Double?,
        val name: String,
)

data class Material(
        val amount: BigDecimal,
        val price: BigDecimal,
        val unit: String,
        val description: String
)

data class Property(
        val name: String,
        val address: Address,
        val managerName: String,
        val companyName: String,
        val companyRegistryCode: String,
)

data class Device(
        val name : String,
        val activities: List<Activity>
)

data class Comment(
        val comment : String
)


data class Address(
        val country: String?,
        val county: String?,
        val city: String?,
        val street: String?,
        val houseNumber: String?,
        val apartmentNumber: String?,
        val postalCode: String?
) {
        fun formattedAddress() : String {
                return "$street $houseNumber, $postalCode, $city"
        }
}

data class Work(
        val price: BigDecimal,
        val total: Long,
        val date: Long,
        val technicianName: String,
)
