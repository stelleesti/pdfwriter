package saago.eu.pdf.common

import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
class util {

    fun formatTime(milliSeconds : Long): String {
        val hours = TimeUnit.MILLISECONDS.toHours(milliSeconds)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(milliSeconds) % 60
        return String.format("%02d:%02d", hours, minutes)
    }
}

