package saago.eu.pdf.common

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/health"], produces = ["application/json"])
class HealthController {

    @GetMapping
    fun get(): ResponseEntity<Any> {
        return ResponseEntity.ok().body(HealthStatus("ok"))
    }
}

data class HealthStatus(
        val status: String
)
